package com.example.handsontest.Model.Entity;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class processEmployeeTest {

    @Test
    void calculateAnnualSalary() {
        Employee emp=new Employee(3,"Ana","\"HourlySalaryEmployee\"",1,"Administrador",null,10000.0,50000.0);
        emp=processEmployee.calculateAnnualSalary(emp);
        assertEquals(emp.annualSalary,14400000);
    }
}