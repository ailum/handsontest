package com.example.handsontest.View;


import com.example.handsontest.Model.Entity.Employee;
import com.example.handsontest.Model.Entity.processEmployee;

import java.util.ArrayList;

public class ViewGenerator {

    public String viewProducts(ArrayList<Employee> employees){
        String r = "<div align='center'><h2>Empleados Encontrados </h2><br>";
                r +="<a href='index.html'> Regresar </a>";
        for (Employee e:employees) {
                e=processEmployee.calculateAnnualSalary(e);
                r += "<div align='center'><table border='1px'><tr><th>Id -> " + e.getId();
                r += "</th></tr>"
                        + "<tr><td>Name:" + e.getName() + "</td></tr>"
                        + "<tr><td>ContractTypeName:" + e.getContractTypeName() + "</td></tr>"
                        + "<tr><td>RoleId:" + e.getRoleId() + "</td></tr>"
                        + "<tr><td>RoleName:" + e.getRoleName() + "</td></tr>"
                        + "<tr><td>RoleDescription:" + e.getRoleDescription() + "</td></tr>"
                        + "<tr><td>HourlySalary:" + e.getHourlySalary() + "</td></tr>"
                        + "<tr><td>MonthlySalary:" + e.getMonthlySalary() + "</td></tr>"
                        + "<tr><td>AnnualSalary:" + e.getAnnualSalary() + "</td></tr></table></div><br>";
            }

        return r;
    }
}
