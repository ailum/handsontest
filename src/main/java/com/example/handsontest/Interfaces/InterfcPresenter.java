package com.example.handsontest.Interfaces;

import com.example.handsontest.Model.Entity.Employee;
import java.util.ArrayList;

public interface InterfcPresenter {
	public ArrayList<Employee> getEmployees(int id);
	public boolean updateEmployeesApi() ;
}
