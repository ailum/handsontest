package com.example.handsontest.Interfaces;

import com.example.handsontest.Model.Entity.Employee;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;

public interface InterfcInteractor {
	public ArrayList<Employee> getEmployees(int id) ;
	public boolean updateEmployeesApi() ;
}
