package com.example.handsontest.Interfaces;

import java.util.ArrayList;

public interface DaoEmployees<Employee> {

    ArrayList<Employee> get(int id);
    ArrayList<Employee> getAll();
}
