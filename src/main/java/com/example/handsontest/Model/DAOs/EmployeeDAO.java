package com.example.handsontest.Model.DAOs;


import com.example.handsontest.Interfaces.DaoEmployees;
import com.example.handsontest.Model.Entity.Employee;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class EmployeeDAO implements DaoEmployees<Employee> {

	ArrayList<Employee> employees=new ArrayList<Employee>();
	
	public boolean GETRequest() {
		employees.clear();
		try {

            URL url = new URL("http://masglobaltestapi.azurewebsites.net/api/employees");//your url i.e fetch data from .
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }
            
            System.out.println(conn.getContent().toString());
            
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
            while ((output = br.readLine()) != null) {
            	
            	String[] emp=output.split("}");
            	
            	for(int e=0;e<emp.length-1;e++) {
            		String s=emp[e].substring(2,emp[e].length());
            		String[] dataEmp=s.split(",");

            		int id=Integer.parseInt(dataEmp[0].split(":")[1]);
            		String name=dataEmp[1].split(":")[1];
            		String contractType=dataEmp[2].split(":")[1];
            		int roleId=Integer.parseInt(dataEmp[3].split(":")[1]);
            		String roleName=dataEmp[4].split(":")[1];
            		String roleDescription=dataEmp[5].split(":")[1];
            		double hourlySalary=Double.parseDouble(dataEmp[6].split(":")[1]);
            		double monthlySalary=Double.parseDouble(dataEmp[7].split(":")[1]);
            	
                	Employee new_emp=new Employee(id,name, contractType, roleId,roleName,roleDescription,hourlySalary, monthlySalary); 
                	employees.add(new_emp);
            	}
            }
            System.out.println(employees.size());
            conn.disconnect();
            return true;

        } catch (Exception e) {
            System.out.println("Exception in NetClientGet:- " + e);
            return false;
        }

	}
	
	public boolean POSTRequest() throws IOException {

		String POST_PARAMS ="";
		for (Employee e:employees) {
			POST_PARAMS += "{\n" + "\"id\":"+e.getId()+",\r\n" +
			        "    \"name\": \""+ e.getName()+"\",\r\n" +
			        "    \"contractTypeName\": \""+e.getContractTypeName()+"\",\r\n" +
			        "    \"roleId\": "+e.getRoleId()+",\r\n"+
			        "    \"roleName\": \""+e.getRoleName()+"\",\r\n"+
			        "    \"roleDescription\": \""+e.getRoleDescription()+"\",\r\n"+
			        "    \"hourlySalary\": "+e.getHourlySalary()+",\r\n"+
			        "    \"monthlySalary\": "+e.getMonthlySalary()+",\r\n"+
			        "    \"AnnualSalary\": "+e.getAnnualSalary()+
			        "\n},";
		}
	   
	    System.out.println(POST_PARAMS);
	    URL obj = new URL("http://localhost:8000/employee/");
	    HttpURLConnection postConnection = (HttpURLConnection) obj.openConnection();
	    postConnection.setRequestMethod("POST");
	    postConnection.setRequestProperty("User-Agent", "application/json");

	    postConnection.setDoOutput(true);
	    OutputStream os = postConnection.getOutputStream();
	    os.write(POST_PARAMS.getBytes());
	    os.flush();
	    os.close();


	    int responseCode = postConnection.getResponseCode();
	    System.out.println("POST Response Code :  " + responseCode);
	    System.out.println("POST Response Message : " + postConnection.getResponseMessage());

	    if (responseCode == HttpURLConnection.HTTP_CREATED) { //success
	        BufferedReader in = new BufferedReader(new InputStreamReader(
	            postConnection.getInputStream()));
	        String inputLine;
	        StringBuffer response = new StringBuffer();

	        while ((inputLine = in .readLine()) != null) {
	            response.append(inputLine);
	        } in .close();

	        // print result
	        System.out.println(response.toString());
	        return true;
	    } else {
	        System.out.println("POST NOT WORKED");
	        return false;
	    }
	}
	
	
	@Override
	public ArrayList<Employee> get(int id) {
		List<Employee> result=employees.stream().filter(e->e.getId()==id).collect(Collectors.toList());
		return (ArrayList<Employee>) result;
	}

	@Override
	public ArrayList<Employee> getAll() {
		return employees;
	}

}
