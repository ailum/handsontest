package com.example.handsontest.Model;

import com.example.handsontest.Interfaces.InterfcInteractor;
import com.example.handsontest.Interfaces.InterfcPresenter;
import com.example.handsontest.Model.DAOs.EmployeeDAO;
import com.example.handsontest.Model.Entity.Employee;
import com.example.handsontest.Presenters.Presenter;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;


public class Interactor implements InterfcInteractor {

	private InterfcPresenter presenter;
	EmployeeDAO dao=new EmployeeDAO();
	
	public Interactor(Presenter presenter) {
		this.presenter=presenter;
	}
	
	@Override
	public ArrayList<Employee> getEmployees(int id) {
		boolean state=dao.GETRequest();
		if(state){
			ArrayList<Employee> emp=dao.get(id);
			if(emp.size()>0){
				return emp;
			}else{
				return dao.getAll();
			}
		}
		return null;
	}

	@Override
	public boolean updateEmployeesApi() {
		try {
			return dao.POSTRequest();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
}
