package com.example.handsontest.Model.Entity;

public class Employee {
	
	int id;
	String name;
	String contractTypeName;
	int roleId;
	String roleName;
	String roleDescription;
	double hourlySalary;
	double monthlySalary;
	double annualSalary;
	
	public Employee(int id, String name, String contractType, int roleId, String roleName, String roleDescription
			,double hourlySalary, double monthlySalary){
		
		this.id=id;
		this.name=name;
		this.contractTypeName=contractType;
		this.roleId=roleId;
		this.roleName=roleName;
		this.roleDescription=roleDescription;
		this.hourlySalary=hourlySalary;
		this.monthlySalary=monthlySalary;
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContractTypeName() {
		return contractTypeName;
	}

	public void setContractTypeName(String contractTypeName) {
		this.contractTypeName = contractTypeName;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleDescription() {
		return roleDescription;
	}

	public void setRoleDescription(String roleDescription) {
		this.roleDescription = roleDescription;
	}

	public double getHourlySalary() {
		return hourlySalary;
	}

	public void setHourlySalary(double hourlySalary) {
		this.hourlySalary = hourlySalary;
	}

	public double getMonthlySalary() {
		return monthlySalary;
	}

	public void setMonthlySalary(double monthlySalary) {
		this.monthlySalary = monthlySalary;
	}

	public int getId() {
		return id;
	}

	public double getAnnualSalary() {
		return annualSalary;
	}

	public void setAnnualSalary(double annualSalary) {
		this.annualSalary = annualSalary;
	}

}
