package com.example.handsontest.Model.Entity;

public class processEmployee {
	
	public static Employee calculateAnnualSalary(Employee emp) {
		double annualS;
		if(emp.getContractTypeName().equals("\"HourlySalaryEmployee\"")) {
			annualS= 120*emp.getHourlySalary()*12;
		}else {
			annualS= emp.getMonthlySalary()*12;
		}
		
		emp.setAnnualSalary(annualS);
		return emp;
		
	}

}
