package com.example.handsontest;

import com.example.handsontest.Interfaces.InterfcPresenter;
import com.example.handsontest.Model.Entity.Employee;
import com.example.handsontest.Presenters.Presenter;
import com.example.handsontest.View.ViewGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.ArrayList;

@SpringBootApplication
@RestController
public class HandsOnTestApplication {

    private InterfcPresenter presenter;
    private ViewGenerator viewgenerator = new ViewGenerator();

    public static void main(String[] args) {
        SpringApplication.run(HandsOnTestApplication.class, args);
    }

    @PostConstruct
    private void postInit()
    {
        System.out.println("constructed");
        presenter=new Presenter();
    }

    @GetMapping("/wellcome")
    public String getId(@RequestParam(value = "username") String id_){
        int id;
        if(id_.equals("")){
            id=0;
        }else{
            id=Integer.parseInt(id_);
        }
        System.out.println(id);
        ArrayList<Employee> emps=presenter.getEmployees(id);
        return viewgenerator.viewProducts(emps);
    }

}
