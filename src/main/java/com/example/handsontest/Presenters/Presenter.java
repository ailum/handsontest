package com.example.handsontest.Presenters;

import com.example.handsontest.Interfaces.InterfcInteractor;
import com.example.handsontest.Interfaces.InterfcPresenter;
import com.example.handsontest.Model.Entity.Employee;
import com.example.handsontest.Model.Interactor;
import com.example.handsontest.HandsOnTestApplication;

import java.util.ArrayList;

public class Presenter implements InterfcPresenter {

	private InterfcInteractor interactor;
	
	public Presenter() {
		interactor=new Interactor(this);
	}

	@Override
	public ArrayList<Employee> getEmployees(int id) {
		return interactor.getEmployees(id);
	}

	@Override
	public boolean updateEmployeesApi() {
		return interactor.updateEmployeesApi();
	}

}
